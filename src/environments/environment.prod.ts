export const environment = {
  production: true,
  config: {
    serviceUrl:'http://pi-server:23380/',
    audioFileExtensions:['.mp3','.ogg','.m3u','.mov','.mp4','.avi','.mkv'],
    audioStreamingRelativePath:'/assets',
    videoFileExtensions:['.mov','.mkv','.avi']
  }
};
