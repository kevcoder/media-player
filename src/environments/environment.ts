// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  config: {
    serviceUrl:'http://localhost:3001/',
    // serviceUrl:'http://pi-server:23380/',
    audioFileExtensions:['.mp3','.ogg','.m3u','.mov','.mp4','.avi','.mkv'],
    audioStreamingRelativePath:'/assets',
    videoFileExtensions:['.mov','.mkv','.avi']

  }
};
