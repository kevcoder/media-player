import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpResponse } from '@angular/common/http';

import { Tree } from '../model/tree';
import { Folder } from '../model/folder';
import { Utility } from '../model/utility';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class TreeService {

  private _url = '/files';
  constructor(private _http: HttpClient) { }

  public getTree(rootFolder): Observable<Folder> {
    let url = Utility.combine([environment.config.serviceUrl, this._url, encodeURIComponent(rootFolder)]);
    return this._http.get<Tree>(url, { observe: 'response' }).map(val => val.body.tree);

  };

}