import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observer, Observable } from 'rxjs/Rx';
import { HttpResponse } from '@angular/common/http';

import { Utility } from '../model/utility';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';

import { PlayList } from '../model/play-list';
import { PlayListItem } from '../model/play-list-item';
import { observableToBeFn } from 'rxjs/testing/TestScheduler';

@Injectable()
export class PlayListService {

  private _url = '/playlists';
  @Output() evtAddedToPlayList = new EventEmitter<PlayListItem>();

  constructor(private _http: HttpClient) { }

  public getPlaylist(playlistId: string): Observable<PlayList> {

    let url = Utility.combine([environment.config.serviceUrl, this._url, encodeURIComponent(playlistId)]);
    return this._http
      .get<PlayList>(url, { observe: 'response' })
      .map(val => val.body);

  };

  public deletePlayList(playListId: string): Observable<PlayList> {
    let url = Utility.combine([environment.config.serviceUrl, this._url, encodeURIComponent(playListId)]);
    return this._http
      .delete<PlayList>(url)
      .map(val => val);
  }

  public savePlaylist(playlist: PlayList): Observable<string> {
    let url = Utility.combine([environment.config.serviceUrl, this._url]);
    return this._http
      .post<any>(url, playlist)
      .map(val => val.playlistId);
  }

  public addToPlaylist(media: PlayListItem): void {
    console.info('called addToPlaylist with ', media);
    this.evtAddedToPlayList.emit(media);
  }
}