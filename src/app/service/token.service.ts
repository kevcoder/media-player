import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpResponse } from '@angular/common/http';

import { Utility } from '../model/utility';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';
import * as libJwt from 'jsonwebtoken';
import { TokenPayload } from '../model/token-payload';
import { Token } from '../model/token';
import { User } from '../model/user';


@Injectable()
export class TokenService {

  private _url = '/users';
  private _localStorageKey: string = "token";
  private _localStorageUserIdKey: string = "jwt.userid";
  private _localStorageUserName: string = "jwt.username";


  constructor(private _http: HttpClient) { }

  public getUser(user: string, pwd: string): Observable<User> {
    let url = Utility.combine([environment.config.serviceUrl, this._url, encodeURIComponent(user)]);
    return this._http.get<User>(url);
  }

  public saveUser(user: User): Observable<string> {
    let url = Utility.combine([environment.config.serviceUrl, this._url]);
    return this._http
      .post(url, user,{responseType:'text'})
  }

}
