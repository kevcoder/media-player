import { FileType } from "./file-type.enum";
import * as _ from 'lodash';


export class Folder {
    public path: string;
    public name: string;
    public size: number;
    public type: string;
    public children?: Folder[];
    public extension?: string;
}