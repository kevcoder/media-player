export class Token {
    public issuer:string = '';
    public token: string = '';
    public type:string = '';
}
