
export class Utility {
    static combine(urls: string[]) {
        let final = '';
        if (urls) {
            if (urls.length && urls.length >= 1) {
                urls.forEach((val, idx) => {
                    //idx 0 is the site root: remove any trailing slashes
                    if (idx == 0) {
                        final = val;
                        if (final.endsWith('/')) { final = final.substring(0, final.length - 1); }
                    }
                    else {
                        //for the remaining indexes pre-pend a slash if missing and then append to final
                        let x = val;
                        if (!x.startsWith('/')) { x = '/' + x; }
                        final += x;
                    }
                });
            }
        }
        return final;
    }
}
