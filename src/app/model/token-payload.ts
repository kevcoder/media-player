export class TokenPayload {
    public playlistId: string[] = [];
    public userId: string = '';
    public defaultPath: string = '';
    public exp: number = 0;
    public sub: string = '';
    public iss: string = '';
    public iat: string = '';
}
