import { PlayList } from "./play-list";
import { PlaybackSettings } from "./playback-settings";

export interface User {
    _id: string,
    email: string,
    userId: string,
    playLists: PlayList[],
    playBackSettings: PlaybackSettings
}
