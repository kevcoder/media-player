export class PlaybackSettings {
    public repeat: boolean = true;
    public shuffle: boolean = false;
}
