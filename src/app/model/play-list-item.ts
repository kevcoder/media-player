import { FileType } from "./file-type.enum";

export class PlayListItem {

    public fullUrl:string;
    public name:string;
    public path: string; 
    public size: number;
    public extension: string;
    public type: FileType;

}
