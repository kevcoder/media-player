import { PlayListItem } from './play-list-item';
export class PlayList {
    public playlistName: string = '';
    public items: PlayListItem[] = [];
}
