import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { PlayListComponent } from './play-list/play-list.component';

import { TreeService } from './service/tree.service';
import { PlayListService } from './service/play-list.service';
import { TokenService } from './service/token.service';

import * as _ from 'lodash';
import * as libJwt from 'jsonwebtoken';
import { PlayerComponent } from './player/player.component';
import { TreeComponent } from './tree/tree.component';
import { TreeModule } from 'angular-tree-component';


@NgModule({
  declarations: [
    AppComponent,
    PlayListComponent,
    PlayerComponent,
    TreeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    TreeModule
  ],
  providers: [TreeService, PlayListService, TokenService],
  bootstrap: [AppComponent]
})
export class AppModule { }
