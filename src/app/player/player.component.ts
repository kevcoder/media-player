import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { PlayListService } from '../service/play-list.service';
import { TokenService } from '../service/token.service';
import { PlayListItem } from '../model/play-list-item';
import { Utility } from '../model/utility';
import { environment } from '../../environments/environment';
import { PlayListComponent } from '../play-list/play-list.component';
import { FileType } from '../model/file-type.enum';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

  ngOnInit() {
  }

  public currentAction: string = 'waiting...';
  constructor(private _playListSvc: PlayListService, private _tokenSvc: TokenService) { }

  @ViewChild('videoPlayer') private videoPlayer: any;

  @Output() evtMediaStarted = new EventEmitter<PlayListItem>();
  @Output() evtMediaPaused = new EventEmitter<PlayListItem>();
  @Output() evtMediaEnded = new EventEmitter<PlayListItem>();
  @Output() evtMediaLoaded = new EventEmitter<PlayListItem>();
  @Output() evtMediaBuffering = new EventEmitter<PlayListItem>();

  @Input() autoPlay: boolean = false;
  @Input() width: string= '100%';
  @Input() height: string = '30px';

  public get title() {
    return (this._media) ? this._media.name : '';
  }

  private _media: PlayListItem = new PlayListItem();
  @Input() set media(media: PlayListItem) {
    this._media = media;
    if (this._media) {
      this.loadMedia(this.autoPlay);
    }
  }


  private onPlayListLoaded($event) {
    this.currentAction = `loaded ...`;
    this.evtMediaLoaded.emit(this._media);
  }

  private onPlaybackEnded($event) {
    this.currentAction = `stopped ...`;
    this.evtMediaEnded.emit(this._media);
  }

  private onPlaybackStarted($event) {
    this.currentAction = 'playing ...';
    this.evtMediaStarted.emit(this._media);
  }

  private onPlaybackPaused($event) {
    this.currentAction = 'paused ...';
    this.evtMediaPaused.emit(this._media);
  }

  private onBuffering($event) {
    this.currentAction = 'buffering ...';
    this.evtMediaBuffering.emit(this._media);
  }


  //begin utility methods

  private getFullUrlToFile(filePath: string) {
    let thePath = filePath;
    if (filePath && filePath.length > 0) {
      thePath = Utility.combine([environment.config.serviceUrl, environment.config.audioStreamingRelativePath, filePath]);
    }
    return thePath;
  }

  //end utility methods


  //playlist related stuff
  private loadMedia(play: boolean) {
    play = (play || false);

    this._media.fullUrl = this.getFullUrlToFile(this._media.path);
    if (this.videoPlayer.nativeElement.src != this._media.fullUrl) {
      this.videoPlayer.nativeElement.src = this._media.fullUrl;
      if (environment.config.videoFileExtensions.indexOf(this._media.extension.trim().toLowerCase()) >= 0) {
        this.height = '300px';        
      }
      else{
        this.height = '30px';
      }
      this.onPlayListLoaded(this._media);
    }
    if (play) {
      this.stopMedia();
      this.playMedia();
    }
  }

  private playMedia() {
    this.videoPlayer.nativeElement.play();

  }
  private stopMedia() { this.videoPlayer.nativeElement.pause(); }

}
