import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';

import * as _ from 'lodash';
import * as moment from 'moment';

import { TreeService } from './service/tree.service';
import { PlayListService } from './service/play-list.service';
import { TokenService } from './service/token.service';

import { Folder } from './model/folder';
import { Tree } from './model/tree';

import { environment } from '../environments/environment';
import { Utility } from './model/utility';
import { PlayList } from './model/play-list'
import { PlaybackSettings } from './model/playback-settings';
import { PlayListItem } from './model/play-list-item';
import { PlayListComponent } from './play-list/play-list.component';
import { TokenPayload } from './model/token-payload';
import { fadeInContent } from '@angular/material';
import { User } from './model/user';
import { FileType } from './model/file-type.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    this.getUser();
    this.getFolderContents('');
  }

  private userName: string = "5a9c8b6056bb95ff835c88e0";
  private fullName: string = '';

  private playBackSettings: PlaybackSettings = new PlaybackSettings();
  public tree: Folder;
  public treeIsReady: boolean = false;
  private playListIsReady: boolean = false;
  private autoPlaySelectedMedia: boolean = false;

  public selectedMedia: PlayListItem = null;
  private selectedPlayList: PlayList = null;
  public user: User;
  public playLists: PlayList[] = [];


  constructor(private _treeSvc: TreeService, private _tokenSvc: TokenService, private _playlistSvc: PlayListService) { }



  //token related services
  private getUser() {
    this.playListIsReady = false;
    this._tokenSvc.getUser(this.userName, '')
      .subscribe((resp: User) => {
        this.user = resp;
      }
        , (err) => {
          console.error('getToken returned err', err);
        });
  }
  private saveUser() {
    this._tokenSvc.saveUser(this.user)
      .subscribe(() => {
        this.getUser();
      }, (err: any) => {
        console.error('failed to save user: error', err);
      });
  }

  getFolderContents(rootFolder: string) {
    this.treeIsReady = false;
    this._treeSvc.getTree(rootFolder)
      .subscribe((resp: Folder) => {
        this.tree = this.filterFilesByType(environment.config.audioFileExtensions, resp);
        this.treeIsReady = (this.tree) ? true : false;
      });
  };


  getParentFolder(currentFolder: string) {
    let parent = '';
    if (currentFolder && currentFolder.length > 0) {

      let lastSep = currentFolder.lastIndexOf('/');
      if (lastSep >= 0)
        parent = currentFolder.slice(0, lastSep);

    }
    return this.getFolderContents(parent);
  };

  private filterFilesByType(extensions: string[], folder: Folder) {
    if (folder) {
      if (folder.children) {
        folder.children = _.filter(folder.children, (f) => {
          return f.type === 'directory' || _.includes(extensions, f.extension);
        });
      }
    }
    return folder;
  }

  private onMediaLoaded(media: PlayListItem, autoPlay: boolean) {
    this.selectedMedia = media;
    this.autoPlaySelectedMedia = autoPlay;
  }

  addToPlaylist(media: PlayListItem) {
    let newPlaylist;
    if (this.selectedPlayList) {
      this.selectedPlayList.items.push(media);
      newPlaylist = this.selectedPlayList;
    }
    else {

      let userDefinedPlaylistName = prompt('New Play List Name', )

      this.createNewPlaylist(userDefinedPlaylistName || `playlist ${moment().format('YYYYmmDDhhmmss')}`, [media]);
    }

  }

  private createNewPlaylist(name: string, mediaItems: PlayListItem[]) {
    let newPlaylist;
    if (!name || !mediaItems || mediaItems.length == 0)
      return newPlaylist;
    else {

      newPlaylist = new PlayList();
      newPlaylist.items = mediaItems;
      newPlaylist.playlistName = name;
      this.user.playLists.push(newPlaylist);
      this.saveUser();
      alert(`Created new playlist ${newPlaylist.playlistName}. You can now add new items to that playlist.`)

    }
  }

  public populatePlaylist(folder: Folder) {
    let items: PlayListItem;
    if (!folder || !folder.children || !_.some(folder.children, { type: 'file' }))
      return;
    else {
      if (confirm(`create a playlist for all media in ${folder.name}`)) {
        this.createNewPlaylist(folder.name, _.map(folder.children, (val) => {
          if (val.type === 'file' && _.includes(environment.config.audioFileExtensions, val.extension || '')) {
            let p = new PlayListItem();
            p.extension = val.extension;
            p.name = val.name;
            p.path = val.path;
            p.size = val.size;
            p.type = FileType[val.type];
            return p;
          }
        }));
      }
    }
  }

  public onPlayListLoaded(playList: PlayList) {
    this.selectedPlayList = playList;
  }

  public onPlayListSaved(playList: PlayList) {
    _.remove(this.user.playLists, { playlistName: playList.playlistName });
    if (playList.items.length)
      this.user.playLists.push(playList);
    this.saveUser();
  }

  public onMediaEnded(playListItem: PlayListItem) {
    if (playListItem) {
      console.info(`finished playing ${playListItem.name}`);
      this.pickNextSongIndex(playListItem);
    }
  }


  //manage playback settings

  private pickNextSongIndex(media: PlayListItem) {
    let selectedMediaIdx = _.findIndex(this.selectedPlayList.items, (val) => val.name === media.name);
    let atEndOfPlaylist = selectedMediaIdx == (this.selectedPlayList.items.length - 1);

    if (atEndOfPlaylist && !this.user.playBackSettings.repeat)
      return;

    let nextIdx = 0;

    if (this.user.playBackSettings.shuffle) {

      let excludedIndexes: number[] = [selectedMediaIdx];
      let eligibleIndexes = new Array(this.selectedPlayList.items.length)
        .fill(0)
        .map((x, y) => x + y);

      //remove the excluded
      _.remove(eligibleIndexes, (val, idx) => _.includes(excludedIndexes, val));

      //pick a random media file that's not the current media
      if (eligibleIndexes.length > 1)
        nextIdx = this.pickRandomNumber(0, eligibleIndexes.length);
      else
        nextIdx = eligibleIndexes[0];
    }
    else {
      nextIdx = (atEndOfPlaylist || ((selectedMediaIdx + 1) > (this.selectedPlayList.items.length - 1))) ? 0 : selectedMediaIdx + 1;
    }

    this.selectedMedia = this.selectedPlayList.items[nextIdx];
    //load this song
  }

  private pickRandomNumber(min: number, max: number) {
    return Math.floor(Math.random() * ((max + 1) - min)) + min;
  }

}
