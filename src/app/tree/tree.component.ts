import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { on } from 'cluster';
import { Tree } from '../model/tree';
import { TreeService } from '../service/tree.service';
import { Folder } from '../model/folder';
import { PlayListItem } from '../model/play-list-item';
import { PlayList } from '../model/play-list';
import * as _ from 'lodash';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html'
})
export class TreeComponent implements OnInit {

  constructor(private treeSvc: TreeService) { }

  ngOnInit() {
  }

  @Input() set tree(val: Folder) {
    if (val) {
      this.nodes = val.children;
    }
  }
  @Input() treeIsReady: boolean = false;
  @Output() onFolderClick = new EventEmitter<string>();
  @Output() onParentFolderClick = new EventEmitter<string>();
  @Output() onAddingMediaToPlaylist = new EventEmitter<PlayListItem>();
  @Output() onPopulatingPlaylist = new EventEmitter<Folder>();

  nodes: any[] =  [];
  options: any = {};


  hasMediaFiles(fldr: Folder, fileType: string) {
    if (!fldr || !fldr.children)
      return false;
    else
      return _.some(fldr.children, { type: fileType });
  }
private parse(fldr: Folder){
;
}

}
