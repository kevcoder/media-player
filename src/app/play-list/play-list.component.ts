import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as _ from 'lodash';

import { PlayList } from '../model/play-list';
import { PlayListItem } from '../model/play-list-item';
import { PlaybackSettings } from '../model/playback-settings';

@Component({
  selector: 'app-play-list',
  templateUrl: './play-list.component.html',
  styleUrls: ['./play-list.component.css']
})
export class PlayListComponent implements OnInit {

  ngOnInit() { }

  private _playLists: PlayList[] = [];
  @Input() public set playLists(injectedPlaylists: PlayList[]) {
    this.playListNeedsSaving = false;
    this._playLists = injectedPlaylists;
  }
  public get playLists() {
    return this._playLists;
  }
  @Input() playBackSettings: PlaybackSettings;


  @Output() onMediaSelected = new EventEmitter<PlayListItem>();
  @Output() onPlaylistSelected = new EventEmitter<PlayList>();
  @Output() onShuffleToggled = new EventEmitter<boolean>();
  @Output() onRepeatToggled = new EventEmitter<boolean>();
  @Output() onPlayListSaved = new EventEmitter<PlayList>();

  public playList: PlayList = null;

  public playListNeedsSaving: boolean = false;

  //playlist management
  public addToPlaylist(playListEntry: PlayListItem) {
    if (_.findIndex(this.playList.items, playListEntry) < 0) {
      let pl = this.playList;

      pl.items.push(playListEntry);
      this.playListNeedsSaving = true;

      this.playList = pl;
    }
  }

  public clearPlaylist() {
    this.playList.items = [];
    this.playListNeedsSaving = true;
  }

  public savePlaylist(): void {
    if (this.playList){
      this.onPlayListSaved.emit(this.playList);
      this.playList = null;
      
    }
  }

  public onSelectedPlaylist(playListId: string) {
    this.playList = _.find(this.playLists, { playlistName: playListId });
    this.playListNeedsSaving = false;
    this.onPlaylistSelected.emit(this.playList || null);
  }

  public onSelectedMedia(media: PlayListItem) {
    this.onMediaSelected.emit(media);
  }

  public toggleRepeatPlayback() {
    this.playBackSettings.repeat = !this.playBackSettings.repeat;
    this.onShuffleToggled.emit(this.playBackSettings.repeat);
  }

  public toggleShuffle() {
    this.playBackSettings.shuffle = !this.playBackSettings.shuffle
    this.onRepeatToggled.emit(this.playBackSettings.shuffle);
  }
}
